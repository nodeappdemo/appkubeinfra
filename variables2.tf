variable "resource_group_name" {
  description = "resource group name"
}

variable "location" {
  description = "Default Location"
}

variable "vnet_cidr" {
  description = "VNET CIDR"
}

variable "subnet_name" {
  type        = "list"
  description = "List of subnets"
}


variable "subnet_cidr" {
  type        = "list"
  description = "Subnet CIDR"
}

variable "enable_nic" {
  type = bool
}