
variable "prefix" {
  description = "Environment prefix for the resources created in the specified Azure Resource Group"
}

variable "location" {
  default     = "centralus"
  description = "The location for the AKS deployment"
}


variable "admin_username" {
  default     = "azureuser"
  description = "The username of the local administrator to be created on the Kubernetes cluster"
}

variable "agents_size" {
  default     = "Standard_B2s"
  description = "The default virtual machine size for the Kubernetes agents"
}

variable "agents_count" {
  description = "The number of Agents that should exist in the Agent Pool"
  default     = 1
}

variable "kubernetes_version" {
  description = "Version of Kubernetes to install"
  default     = "1.13.12"
}

variable "public_ssh_key" {
  description = "A custom ssh key to control access to the AKS cluster"
  default     = ""
}

variable "CLIENT_ID" {
  description = "The Client ID (appId) for the Service Principal used for the AKS deployment"
}

variable "CLIENT_SECRET" {
  description = "The Client Secret (password) for the Service Principal used for the AKS deployment"
}